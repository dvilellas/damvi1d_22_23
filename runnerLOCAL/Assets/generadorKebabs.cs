using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generadorKebabs : MonoBehaviour
{
    public GameObject menjar;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(nouKebab());
    }

    IEnumerator nouKebab()
    {
        while (true)
        {
            GameObject kebab = Instantiate(menjar);
            kebab.transform.position = new Vector2(10, Random.Range(0, 7));
            yield return new WaitForSeconds(2);
        }

    }
}
