using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kebabController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(this.GetComponent<Transform>().position.x < -7)
        {
            Destroy(this.gameObject);
        }
    }
}
